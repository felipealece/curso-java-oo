package capitulo_4;

import capitulo_3.Exercicio_3_7_Funcionario;

public class Exercicio_4_6_Empresa {
	
	private Exercicio_3_7_Funcionario[] funcionarios;
	private String CNPJ;


	void setFuncionario(){
		this.funcionarios = new Exercicio_3_7_Funcionario[10];

	}


	void adiciona(Exercicio_3_7_Funcionario f){
		for(int i=0 ; i < funcionarios.length; i++){ 
			if(this.funcionarios[i] == null){
				this.funcionarios[i] = f;
				break;
			} 
		}		
	}

	void exibir(){	
		funcionarios[0].exibirSalario();
		funcionarios[1].exibirSalario();
		funcionarios[2].exibirSalario();
		funcionarios[3].exibirSalario();
	}

	void mostraFuncionario(){
		for(int i=0 ; i < funcionarios.length; i++){
			if (funcionarios[i] != null){ 
				System.out.println("Funcion�rio na posi��o " + i + " � " + funcionarios[i].getNome() +"\n");
				}else
					break;	 
		}		
	}

	void mostraFuncionarioEach(){
		for(Exercicio_3_7_Funcionario f : funcionarios){
			if (f != null){
				System.out.println(f.getNome());
			}else
				break;
		}
	}

	void mostraFuncionarioMostra(){
		for(Exercicio_3_7_Funcionario f : funcionarios){
			if (f != null){
				f.mostra();
			}else
				break;
		}
	}                                                                                                               

}
