package capitulo_4;

import capitulo_3.Exercicio_3_7_Funcionario;

public class Exercicio_4_6_TestaEmpresa {
	

	public static void main(String[] args){
		Exercicio_4_6_Empresa empresa = new Exercicio_4_6_Empresa();
		empresa.setFuncionario();

		Exercicio_3_7_Funcionario f1 = new Exercicio_3_7_Funcionario();
		f1.setNome("Felipe");
		f1.setSalario(2000);
		

		Exercicio_3_7_Funcionario f2 = new Exercicio_3_7_Funcionario();
		f2.setNome("Manel");
		f2.setSalario(500);
		

		Exercicio_3_7_Funcionario f3 = new Exercicio_3_7_Funcionario();
		f3.setNome("Leones");
		f3.setSalario(1000);
		
	
		Exercicio_3_7_Funcionario f4 = new Exercicio_3_7_Funcionario();
		f4.setNome("VanDiesel");
		f4.setSalario(999);
		
		
		empresa.adiciona(f1);
		empresa.adiciona(f2);
		empresa.adiciona(f3);
		empresa.adiciona(f4);

		/*empresa.funcionario[0] = f1;
		empresa.funcionario[1] = f2;
		empresa.funcionario[2] = f3;
		empresa.funcionario[3] = f4;
*/
		/*empresa.funcionario[0].exibirSalario();
		empresa.funcionario[1].exibirSalario();
		empresa.funcionario[2].exibirSalario();
		empresa.funcionario[3].exibirSalario();
*/
		empresa.exibir();
		empresa.mostraFuncionario();
		empresa.mostraFuncionarioEach();
		empresa.mostraFuncionarioMostra();	

	}

}
