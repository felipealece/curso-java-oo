package capitulo_3;

public class Exercicio_3_7_Funcionario {
	
	private double salario;
	private String cargo;
	private String RG;
	private String nome;

	public Exercicio_3_7_Funcionario(){

	}

	public Exercicio_3_7_Funcionario(String nome){

		this.nome = nome;	
	}
	
	public void setNome(String nome){
		this.nome = nome;
	
	}
	public String getNome(){
		return this.nome;
	}

	public void setSalario(double salario){
		this.salario = salario;
	
	}
	double getSalario(){
		return this.salario;
	}


		
	public void bonifica(double valor){
		salario += valor;
	}

	public double calculaGanhoAnual(){
		return salario*13;	
	} 

	public void exibirSalario(){
		System.out.println("O sal�rio �:" + salario);
	}

	public void exibirGanhoAnual(){
		System.out.println("O sal�rio �:" + calculaGanhoAnual());
	}

	public void mostra(){
		System.out.println( " O nome � " + nome + ", o RG � "+ RG + ", o seu sal�rio " + salario + " e o seu cargo " + cargo);
	
	}

}
