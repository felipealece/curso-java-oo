package capitulo_3;

public class Exercicio_3_7_Gerenciamento_de_Conta {
	public static void main(String[] args){
	
	Exercicio_3_7_Conta felipe = new Exercicio_3_7_Conta();
	felipe.setSaldo(500000);
	felipe.setNumero(5678);
	felipe.setLimite(1000);
	felipe.setNome("Felipe Costa");

	Exercicio_3_7_Conta ivna = new Exercicio_3_7_Conta();
	ivna.setSaldo(250000);
	ivna.setNumero(1234);
	ivna.setLimite(500);
	ivna.setNome("Ivna Melo");

	
	felipe.imprime();
	ivna.imprime();							
	//--------------------------		
	felipe.transfere(ivna, 50000);
    //-----------------------------

	felipe.imprime();
	ivna.imprime();	
	}
}


