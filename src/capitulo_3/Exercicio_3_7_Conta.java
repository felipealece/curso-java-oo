package capitulo_3;

public class Exercicio_3_7_Conta {
	
		private double numero;
		private double saldo;
		private double limite;
		private String nome;


		double getNumero(){

			return this.numero;
		
		}
		double getSaldo(){
			return this.saldo;
		
		}
		double getLimite(){
			return this.limite;
		
		}
		String getNome(){
			return this.nome;
		}

		void setNumero(double numero){
			this.numero = numero;
		
		}
		void setSaldo(double saldo){
			this.saldo = saldo;
		}
		void setLimite(double limite){
			this.limite = limite;
		}
		void setNome(String nome){
			this.nome = nome;
		
		}

		
		public boolean transfere(Exercicio_3_7_Conta contaRecebe, double valor){
			
			
			contaRecebe.saldo = contaRecebe.saldo + valor;
			this.saldo = this.saldo - valor;
			return true;
		}

		public boolean saca (double valor){
		
			if (valor < this.saldo && valor >0){
				this.saldo = this.saldo - valor;
				return true;
			}
			return false;		
		}

		
		public boolean deposita (double valor){

			if (valor > 0){
				this.saldo = this.saldo + valor;
				return true;		
			}
			return false;
		}	
		
		public void imprime(){

			System.out.println("\n------------------");
						System.out.println("Conta");
						System.out.println("------------------");	
						System.out.println("Dono: " + this.nome + "\n" + "N�mero: " + this.numero + 			"\n" + "Saldo: " + this.saldo + "\n" + "Limite: " + this.limite +"\n");	
		}

}
